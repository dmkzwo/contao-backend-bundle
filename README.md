# Contao Erweiterung für das Backend (dmkzwo/contao-backend-bundle)

Dieses Bundle erweitert die Backend-Funktionalität.

## Shortcuts für die Seitenstruktur (Listenansicht):

* Seite durchsuchen
* Caching
* Index, Follow

## Backend Stylesheets

Eine Datei *files/framework/be.css* wird automatisch zum Backend hinzugefügt.


## ToDo ?

* Weiterleitung alte Urls


## Installation

composer.json

    "repositories": [
        ...
        {
            "type": "vcs",
            "url": "git@gitlab.com:dmkzwo/contao-backend-bundle.git"
        },
        ...
    ],

Composer Install:

    composer require dmkzwo/contao-backend-bundle

Composer Update

    composer update dmkzwo/contao-backend-bundle

* Install-Tool aufrufen
