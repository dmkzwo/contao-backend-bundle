<?php


namespace Dmkzwo\ContaoBackendBundle\Tests;

use Dmkzwo\ContaoBackendBundle\ContaoBackendBundle;
use PHPUnit\Framework\TestCase;

class ContaoBackendBundleTest extends TestCase
{
    public function testCanBeInstantiated()
    {
        $bundle = new ContaoBackendBundle();

        $this->assertInstanceOf('Dmkzwo\ContaoBackendBundle\ContaoBackendBundle', $bundle);
    }
}
