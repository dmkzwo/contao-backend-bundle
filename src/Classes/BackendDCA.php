<?php
/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 12.06.19
 * Time: 10:34
 */

namespace Dmkzwo\ContaoBackendBundle\Classes;


class BackendDCA extends \Backend
{
    public function addToLabel($row, $label, $dc, $imageAttribute)
    {

        $newLabel = $label;

        if (strlen($row['dz_dev_status_1']) && ($row['dz_dev_status_1'] != 'disable')) {
            $newLabel .= ' <div class="dz_dev_status ' . $row['dz_dev_status_1'] . '"></div>';
        }

        $newLabel .= ' <a href="javascript:;" onclick="dzToggleSearch(' . $row['id'] . ');jQuery(this).toggleClass(\'search\');jQuery(this).toggleClass(\'nosearch\');" class="' . (($row['noSearch'] == 1) ? 'no' : '') . 'search">s</a>';
//        $newLabel .= ' <a href="javascript:;" onclick="dzToggleSitemap(' . $row['id'] . ');jQuery(this).toggleClass(\'sitemap\');jQuery(this).toggleClass(\'nositemap\');" class="' . (($row['sitemap'] == 'map_never') ? 'no' : '') . 'sitemap">xml</a>';

        if (strlen($row['robots'])) {
            $robots = explode(',', $row['robots']);
            $newLabel .= ' <a href="javascript:;" onclick="dzToggleIndex(' . $row['id'] . ');jQuery(this).toggleClass(\'index\');jQuery(this).toggleClass(\'noindex\');" class="' . $robots[0] . '">i</a>';
            $newLabel .= '<a href="javascript:;" onclick="dzToggleFollow(' . $row['id'] . ');jQuery(this).toggleClass(\'follow\');jQuery(this).toggleClass(\'nofollow\');" class="' . $robots[1] . '">f</a>';
        }

        if ($row['includeCache']) {
            $newLabel .= ' <a href="javascript:;" onclick="dzToggleCache(' . $row['id'] . ', jQuery(this));" class="' . (($row['cache'] == 0) ? 'no' : '') . 'cache">' . (($row['cache'] == 0) ? 'nc' : $row['cache']) . '</a>';
        } else {
            $newLabel .= ' <a href="javascript:;" onclick="dzToggleCache(' . $row['id'] . ', jQuery(this));" class="parentcache">--</a>';
        }

        $this->import("tl_page");
        return $this->tl_page->addIcon($row, $newLabel, $dc, $imageAttribute);

    }
}
