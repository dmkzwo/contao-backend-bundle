<?php

namespace Dmkzwo\ContaoBackendBundle\Controller;

use Dmkzwo\ContaoBackendBundle\Library\BackendHelper;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;


class AjaxController extends Controller
{

    /**
     * @Route("/dz/backend/{mode}/{id}", name="dz_backend", defaults={"_scope" = "frontend", "_token_check" = false})
     */
    public function ajaxAction($mode, $id)
    {

        $outputData = [];

        if ($mode != '') {

            $pageId = $id;
            //$backendHelper = new BackendHelper();

            if ($mode == 'toggleindex') {
                $outputData = BackendHelper::toggleIndex($pageId);
            }

            if ($mode == 'togglefollow') {
                $outputData = BackendHelper::toggleFollow($pageId);
            }

            if ($mode == 'togglecache') {
                $outputData = BackendHelper::toggleCache($pageId);
            }

            if ($mode == 'togglesearch') {
                $outputData = BackendHelper::toggleSearch($pageId);
            }

            if ($mode == 'togglesitemap') {
                $outputData = BackendHelper::toggleSitemap($pageId);
            }

        }

        return new JsonResponse($outputData);

    }

}