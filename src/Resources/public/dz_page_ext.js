jQuery.noConflict();
var ajCon = 'dz/backend/';

function dzToggleIndex(id) {
  jQuery.get(ajCon+'toggleindex/'+id);
}


function dzToggleFollow(id) {
  jQuery.get(ajCon+'togglefollow/'+id);
}

function dzToggleCache(id, self) {
  jQuery.getJSON(ajCon+'togglecache/'+id,
    function(data){
      var includeCache = data.includeCache;
      var cache = data.cache;
      if (includeCache == '') {
        self.attr('class', 'parentcache');
        self.html('--');
      } else {
        if (cache == '0') {
          self.attr('class', 'nocache');
          self.html('nc');
        } else {
          self.attr('class', 'cache');
          self.html('60');
        }
      }
    }
  );
}

function dzToggleSearch(id) {
  jQuery.get(ajCon+'togglesearch/'+id);
}

function dzToggleSitemap(id) {
  jQuery.get(ajCon+'togglesitemap/'+id);
}
