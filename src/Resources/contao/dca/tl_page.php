<?php

$this->import('BackendUser', 'User');

if (in_array($this->User->username, array('admin', 'volker', 'silja', 'martina', 'michelle', 'maria', 'dennis', 'steph', 'johanna')) && ($this->Input->get('do') == 'page')) {
    $GLOBALS['TL_DCA']['tl_page']['list']['label']['label_callback'] = array(
        'Dmkzwo\\ContaoBackendBundle\\Classes\\BackendDCA',
        'addToLabel'
    );
}
