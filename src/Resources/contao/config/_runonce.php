<?php

class DakksBackendRunOnce extends Controller
{

    /**
     * Initialize the object
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('Database');
    }


    /**
     * Run the controller
     */
    public function run()
    {
//        $this->insertData();
    }


//    private function insertData()
//    {
//        $sql = file_get_contents(\System::getContainer()->getParameter('kernel.project_dir') . DIRECTORY_SEPARATOR . 'vendor/dmkzwo/contao-dakks-as-bundle/src/Resources/contao/config/tl_dz_dakks_as_country.sql');
//        $this->Database->query($sql);
//    }

}


/**
 * Instantiate controller
 */
$objRunOnce = new DakksBackendRunOnce();
$objRunOnce->run();