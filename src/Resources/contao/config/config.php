<?php

if (TL_MODE == 'BE') {
    $GLOBALS['TL_CSS']['dzbackend'] = 'bundles/contaobackend/dz_page_ext.css';
    $GLOBALS['TL_JAVASCRIPT']['dzbackendjquery'] = 'bundles/contaobackend/jquery.min.js';
    $GLOBALS['TL_JAVASCRIPT']['dzbackend'] = 'bundles/contaobackend/dz_page_ext.js';
}