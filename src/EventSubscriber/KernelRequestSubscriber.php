<?php

namespace Dmkzwo\ContaoBackendBundle\EventSubscriber;

use Contao\CoreBundle\Routing\ScopeMatcher;
use Contao\CoreBundle\Framework\ContaoFramework;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class KernelRequestSubscriber implements EventSubscriberInterface
{
    protected $scopeMatcher;
    protected $framework;

    public function __construct(ScopeMatcher $scopeMatcher, ContaoFramework $framework)
    {
        $this->scopeMatcher = $scopeMatcher;
        $this->framework = $framework;
    }

    public static function getSubscribedEvents()
    {
        return [KernelEvents::REQUEST => 'onKernelRequest'];
    }

    public function onKernelRequest(RequestEvent $e): void
    {
        $this->framework->initialize();

        $request = $e->getRequest();

        if ($this->scopeMatcher->isBackendRequest($request)) {
            $backendCss = 'files' . DIRECTORY_SEPARATOR . 'framework' . DIRECTORY_SEPARATOR . 'be.css';
            $backendCssFile = TL_ROOT . DIRECTORY_SEPARATOR . $backendCss;

            if (file_exists($backendCssFile) && is_file($backendCssFile)) {
                $GLOBALS['TL_CSS'][] = $backendCss;
            }
        }
    }
}
