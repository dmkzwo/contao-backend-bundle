<?php

namespace Dmkzwo\ContaoBackendBundle\Library;

class BackendHelper extends \Backend {


    public function addListFunctions($row, $label, $dc, $imageAttribute) {

        $newLabel = $label;

        $newLabel .= ' <a href="javascript:;" onclick="dzToggleSearch('.$row['id'].');$j(this).toggleClass(\'search\');$j(this).toggleClass(\'nosearch\');" class="'.(($row['noSearch'] == 1) ? 'no' : '').'search">s</a>';
        $newLabel .= ' <a href="javascript:;" onclick="dzToggleSitemap('.$row['id'].');$j(this).toggleClass(\'sitemap\');$j(this).toggleClass(\'nositemap\');" class="'.(($row['sitemap_ignore'] == 1) ? 'no' : '').'sitemap">xml</a>';

        if (strlen($row['robots'])) {
            $robots = explode(',', $row['robots']);
            $newLabel .= ' <a href="javascript:;" onclick="dzToggleIndex('.$row['id'].');$j(this).toggleClass(\'index\');$j(this).toggleClass(\'noindex\');" class="'.$robots[0].'">i</a>';
            $newLabel .=  '<a href="javascript:;" onclick="dzToggleFollow('.$row['id'].');$j(this).toggleClass(\'follow\');$j(this).toggleClass(\'nofollow\');" class="'.$robots[1].'">f</a>';
        }

        if ($row['includeCache']) {
            $newLabel .= ' <a href="javascript:;" onclick="dzToggleCache('.$row['id'].', $j(this));" class="'.(($row['cache'] == 0) ? 'no' : '').'cache">'.(($row['cache'] == 0) ? 'nc' : $row['cache']).'</a>';
        } else {
            $newLabel .= ' <a href="javascript:;" onclick="dzToggleCache('.$row['id'].', $j(this));" class="parentcache">--</a>';
        }

        $this->import("tl_page");
        return $this->tl_page->addIcon($row, $newLabel, $dc, $imageAttribute);

    }


    public static function toggleIndex($pageId) {

        $objPage = \Database::getInstance()->prepare("SELECT * FROM tl_page WHERE id=?")
            ->limit(1)
            ->execute($pageId);

        if ($objPage->numRows < 1) {
            return false;
        }

        $robots = $objPage->robots;

        if (($robots == 'index,follow') || ($robots == 'index,nofollow')) {
            $newRobots = str_replace('index', 'noindex', $robots);
        }
        if (($robots == 'noindex,follow') || ($robots == 'noindex,nofollow')) {
            $newRobots = str_replace('noindex', 'index', $robots);
        }

        \Database::getInstance()->prepare("UPDATE tl_page SET robots=? WHERE id=?")
            ->execute($newRobots, $pageId);

        return array('success' => true);

    }


    public static function toggleFollow($pageId) {

        $objPage = \Database::getInstance()->prepare("SELECT * FROM tl_page WHERE id=?")
            ->limit(1)
            ->execute($pageId);

        if ($objPage->numRows < 1) {
            return false;
        }

        $robots = $objPage->robots;

        if (($robots == 'index,follow') || ($robots == 'noindex,follow')) {
            $newRobots = str_replace('follow', 'nofollow', $robots);
        }
        if (($robots == 'index,nofollow') || ($robots == 'noindex,nofollow')) {
            $newRobots = str_replace('nofollow', 'follow', $robots);
        }

        \Database::getInstance()->prepare("UPDATE tl_page SET robots=? WHERE id=?")
            ->execute($newRobots, $pageId);

        return array('success' => true);

    }

    public static function toggleCache($pageId) {

        $objPage = \Database::getInstance()->prepare("SELECT * FROM tl_page WHERE id=?")
            ->limit(1)
            ->execute($pageId);

        if ($objPage->numRows < 1) {
            return false;
        }

        $includeCache = $objPage->includeCache;
        $cache = $objPage->cache;

        if ($includeCache == '') {
            $includeCache = '1';
            $cache = '0';
            //if ($cache == '0') {
            //  $cache = '60';
            //}
        } else {
            if ($cache == '0') {
                $cache = '60';
            } else {
                $includeCache = '';
            }
        }

        \Database::getInstance()->prepare("UPDATE tl_page SET includeCache=?, cache=? WHERE id=?")
            ->execute($includeCache, $cache, $pageId);

        $output = array('includeCache' => $includeCache, 'cache' => $cache);

        return $output;


    }

    public static function toggleSearch($pageId) {

        $objPage = \Database::getInstance()->prepare("SELECT * FROM tl_page WHERE id=?")
            ->limit(1)
            ->execute($pageId);

        if ($objPage->numRows < 1) {
            return false;
        }

        $noSearch = $objPage->noSearch;

        if ($noSearch == '1') {
            $noSearch = '';
        } else {
            $noSearch = '1';
        }

        \Database::getInstance()->prepare("UPDATE tl_page SET noSearch=? WHERE id=?")
            ->execute($noSearch, $pageId);

        return array('success' => true);

    }

    public static function toggleSitemap($pageId) {

        $objPage = \Database::getInstance()->prepare("SELECT * FROM tl_page WHERE id=?")
            ->limit(1)
            ->execute($pageId);

        if ($objPage->numRows < 1) {
            return false;
        }

        $sitemap = $objPage->sitemap;

        if ($sitemap == 'map_default') {
            $sitemap = 'map_never';
        } elseif ($sitemap == 'map_never') {
            $sitemap = 'map_default';
        }

        \Database::getInstance()->prepare("UPDATE tl_page SET sitemap=? WHERE id=?")
            ->execute($sitemap, $pageId);

        return array('success' => true);

    }



}

